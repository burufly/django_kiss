from django_rq import job
from .api import track_kiss


@job('events')
def track(identity, event_name, event_data=None, timestamp=None):
    event_data = event_data or {}

    try:
        track_kiss(identity, event_name, timestamp, **event_data)
    except Exception, exc:
        pass
